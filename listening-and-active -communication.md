# Listening and Active Communication

## Question 1
What are the steps/strategies to do Active Listening?

### Answer
1. Avoid getting distracted by your own thoughts.
2. Do not interrupt the other person.
3. Use door openers.
4. Take notes of important conversations.
5. Paraphrase the speakers words to confirm your understading.
6. Use appropriate body language to exhibit your interest.

## Question 2
According to Fisher's model, what are the key points of Reflective Listening?

### Answer
Reflective Listening is characterized by the following key points:
- Empathy: Show genuine empathy and understanding towards the speaker's perspective and emotions.
- Active Listening: Engage in attentive listening, focusing on both verbal and non-verbal cues of the speaker.
- Paraphrasing: Restate the speaker's words in your own words to demonstrate comprehension and ensure accurate understanding.
- Non-judgmental Attitude: Create a safe and accepting environment, free from criticism or judgment.
- Seeking Clarification: Ask questions or seek clarification to ensure accurate interpretation of the speaker's message.
- Building Trust and Understanding: Foster a sense of trust and rapport with the speaker, allowing for open and honest communication.


## Question 3
What are the obstacles in your listening process?

### Answer
Obstacles in my listening process are:
- Distractions from surroundings.
- Thinking other things while listening.
- Pre-existing beliefs which hinder open minded understanding.

## Question 4
What can you do to improve your listening?

### Answer
Ways that i have figured to improve my listening are:
- Actively try to give more focus.
- Try to eliminate distractions.
- Be open minded to what others re saying.

## Question 5
When do you switch to Passive communication style in your day to day life?

### Answer
- To avoid arguments or fight.
- When in a hurry of facing lack of time.
- To protect others feelings.
- To be respectfull to others.
- When talking with higher officials.


## Question 6
When do you switch into Aggressive communication styles in your day to day life?

### Answer
- When I am feeling angry.
- When trying to win an argument at any cost.
- when tring to assert myself forcefully.
- When feeling threatened or defensive.


### Question 7
When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

### Answer
- When feeling upset but not saying it directly.
- When trying to avoid arguments.
- When wanting attention or control.
- When feeling afraid of criticism or rejection.

### Question 8
How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

### Answer
Following pints could be followed to make communication assertive:
- Be clear and direct in expressing myself.
- Listen actively and show respect for others' perspectives.
- Recognize and respect boundaries.
- Use assertive body language and tone of voice.
- Practice assertive communication skills, such as expressing opinions and setting boundaries.
- Maintain a confident and calm demeanor.
- Seek win-win solutions through open and honest dialogue.
- Stand up for my rights and assert my needs without being aggressive or passive.
