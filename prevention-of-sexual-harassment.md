# Prevention of Sexual Harassment

## Question 1
What kinds of behavior cause sexual harassment?

### Answer
sexual harassment is any unwelcome verbal visual or physical conduct of a
sexual nature that is severe or pervasive and affects working conditions
or creates a hostile work environment. There are generally three forms of
sexual harassment behavior:
- Verbal harassment: This includes comments about clothing, a person's body, sexual or gender-based jokes, remarks requesting sexual favors, or repeatedly asking a person out. It also includes sexual threats, spreading rumors about a person's personal or sexual life, or using foul and obscene language.
- Visual harassment: This can include posters, drawings, pictures, screen savers, cartoons, emails, or texts of a sexual nature.
- Physical harassment: This includes sexual assault, impeding or blocking movement, inappropriate touching such as kissing, hugging, patting, stroking or rubbing, sexual gesturing or even leering or staring.

## Question 2
What would you do in case you face or witness any incident or repeated incidents of such behavior?

### Answer
My reaction towards such an incident would be any of the following according to the situation:
- I will stop the perpetrator and make the victim safe after assessing the situation.
- I will support the victim in whatever way that is possible for me.
- I will reach out for help from others if the situation is bad.
- I will report it to authorities or the police.
- If the victim is not willing to report it to the police, I would try to convince the victim about its importance.
- I will document the incident by keeping notes of time, date, and place and try to collect evidence for further legal proceedings.
- I will consult a lawyer to get legal advice.
