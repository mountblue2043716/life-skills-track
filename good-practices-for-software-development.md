# Good Practices for Software Development

## Question 1
What is your one major takeaway from each one of the 6 sections. So 6 points in total.

### Answer
1. Do all possible things to get maximum clarification of the requirements.
2. Always communicate with the team and let others know about the progress and challenges in the project.
3. When seeking help from others, let the question be precise and explanatory. Do not ask vague questions.
4. Socialise with team members.
5. Be available when someone replies to your message. Things move faster if it's a real-time conversation.
6. Do things with maximum concentration and focus as possible.

## Question 2
Which area do you think you need to improve on? What are your ideas to make progress in that area?

### Answer
The area I think I need to improve is doing things with 100% involvement. This will help me be more productive and complete the work faster to find time for other things like exercise and other hobbies. My ideas to progress in this area are reducing social media, getting proper sleep, scheduling work, and tracking time with productivity tools.