# Learning Process



### Question 1
What is the Feynman Technique? Paraphrase the video in your own words.

##### Answer
There is a common saying that is often attributed to Albert Einstein. It goes like this, "If you can't explain it simply, then you don't understand it well enough". Reversing it reveals a powerful piece of study advice, ie, **"If you want to understand something well, explain it"**. Feynman Technique is all about this concept. 

This technique was named after the famous scientist Feynman, who is commonly known as an "Explainer" because of his excellent capability of explaining complex scientific concepts to common people in simple words. The crux of this concept is that explaining a concept is the best way to learn a concept. We understand the shortcomings of our understanding when we explain things to others.



### Question 2
What are the different ways to implement this technique in your learning process?

##### Answer
The video proposes a four-step process for this technique:

- Write the concept on paper.
- Explain the concept in simple words as if you are teaching someone else.
- Identify where you are weak, go-to resources, and review the concept.
- Challenge yourself to simplify any complicated terms.

These steps allow you to quickly identify areas where your concepts are weak. Work on those areas, and repeat the steps to master any concepts.



### Question 3
Paraphrase the video in detail in your own words.

##### Answer
The video is meant to teach various learning techniques to enhance the learning process. The presenter highlights the importance of taking a break between hours of intense focus. She quotes the examples of Salvador Dali and Thomas Edison, who used to keep an object in their hand when they relax so that the object falls when they sleep and they can get back from sleep with the sound of the object falling on the ground. This way they can continue a cycle of intense focus and relaxation to perform better. 

The presenter proposes the Pomodoro technique to overcome procrastination. This is done by setting a timer for intense focused work before having some fun to relax. Then she continues to highlight the importance of exercise, testing the knowledge, repetition, and practicing to enhance the learning process. She concludes the speech by stating that being a slow learner is not always a bad thing. Even though a slow learner may have to put in extra effort, he may get a different overview or thorough understanding of the topic in the journey of learning. 



### Question 4
What are some of the steps that you can take to improve your learning process?

##### Answer
Following are a few takeaways from the video to improve the learning process:

- Switch state of mind from focus mode (state of mind in intense focus) and diffuse mode (state of mind in a relaxed mode). Take breaks in between work or learning, relax a bit, and come back with full energy.
- Use the Pomodoro technique to overcome procrastination. Set a timer for 25 minutes (or a time period where you can put intense focus), cut off all distractions, and get work done. Get some fun after that before getting back to work again, because learning to relax is also part of the learning process.
- Do exercise on a regular basis. Exercise can increase the ability to both learn and remember.
- Do consider testing yourself. Testing your learning is important as it reveals your weak areas and it also acts as a revision.
- Understanding is not just enough. Understanding combined with practice and repetition is the only way to mastery.



### Question 5
Your key takeaways from the video? Paraphrase your understanding.

##### Answer
The key takeaways from the video are:

- Dont get carried away by the common saying that it takes 10,000 hours to master a skill.
- That applies to mastering a skill, not learning a skill.
- The presenter claims, according to his own research, that it takes just 20 hours of practice to learn a skill.
- The learning progress curve flattens at a point, which means learning progress is high at initial hours of learning.
- Those hours before the flattening of curve is inough to learn a skill and that happens at around 20 hours.
- The major barrier to skill acquisition is not intellectual..., it is emotional. Anyone can learn a skill in with a proper mindset and consistent effort.

The presenter concludes by saying that if you can overcome the feeling of being stupid in the initial hours of learning and continue learning, anyone can learn new skills in 20 hours.



### Question 6
What are some of the steps that you can while approaching a new topic?

##### Answer
The presenter suggests a four step process to to approach learning a new topic:

- **Deconstruct the skill:** Divide the entire task to subtasks so that you can grasp things quicker.
- **Learn inough to self correct:** In the initial stage of learning, try to learn basic things well so that you can improve on it and can self correct when you practice.
- **Remove practice barriers:** Remove distractions like internet and television and focus on the learning.
- **Practice at least 20 hours:** Initially we may feel stupid when we start learning a new skill because we may not do even simple tasks. But don't give up until you reach at least 20 hours.

The presenter says that we can learn any skills if we put 20 hours of consistent effort by following these steps.


