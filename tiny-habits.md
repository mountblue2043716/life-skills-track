# Tiny Habits

## Question 1
Your takeaways from the video (Minimum 5 points).

### Answer
My takeaways from the video are:
1. Environment can have an impact on behavior. A better environment helps in better behavior.
2. It is easy to start with tiny habits and eventually turn them into a daily routine.
3. Don't design for outcomes, but design for the behavior leading to the outcome.
4. Having a trigger for behavior is necessary.
5. One behavior can be a trigger for another so you can plan like I will do this after that.

## Question 2
Your takeaways from the video in as much detail as possible

### Answer
My takeaways from the video are:
- If the task is hard, you need a high level of motivation. So start doing things with small tasks so that you can do them even without motivation.
- Set completion of one action as a trigger to call for another action.
- Shine, celebrate, and reward yourself upon completion of the small task.

## Question 3
How can you use B = MAP to make making new habits easier?

### Answer
B = MAP means that motivation, ability, and a prompt need to happen simultaneously for a behavior to occur. By applying the B = MAP framework to create new habits, you can increase your chances of success. By breaking down the behavior into its parts and addressing motivation, ability, and prompts, you can make the behavior more achievable and increase your chances of making it a habit. Working in teams is a good method to implement B = MAP as it provides motivation and prompts. Befriending with like-minded people is also a similar example.

## Question 4
Why it is important to "Shine" or Celebrate after each successful completion of a habit?

### Answer
Humans like rewards. Rewarding yourself after the completion of a task gives you the motivation to do more of such stuff. In habit formation, the most important skill is the ability to feel good about your behavior as you do it, or immediately after. If you are very good at celebrating, then you start wiring habits into your brain quickly, sometimes in a day or two. This skill gives you habit superpowers.

## Question 5
Your takeaways from the video (Minimum 5 points)

### Answer
My key takeaways from the video are:
1. Consistency is very important. Improving 1% every day consistently for a long period can have a huge result.
2. Repetition is crucial to get the desired effect.
3. Optimizing for the starting line rather than the finish line can make it easier to stick to habits and achieve desired outcomes.
4. Enjoyment and rewards play a significant role in maintaining good habits.
5. Habits shape your identity. Be consistent in repeating positive behaviors.

## Question 6
Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.

### Answer
There are three layers to the process of behavior change, they are identical, processes, and outcomes. The key to building lasting habits is focusing on creating a new identity first. Your current behaviors are simply a reflection of your current identity. What you do now is a mirror image of the type of person you believe that you are.

Many people begin the process of changing their habits by focusing on what they want to achieve. This leads us to outcome-based habits. The alternative is to build identity-based habits. With this approach, we start by focusing on who we wish to become.

## Question 7
Write about the book's perspective on how to make a good habit easier.

### Answer
Following are the book's perspective on making good habits easier:
1. You want to put fewer steps between you and good behavior. Adjust the environment according to that.
2. Make good habits attractive.
3. Make good habits an easy task by priming the environment favorable for that.
4. Make good habits satisfying by giving immediate rewards.

## Question 8
Write about the book's perspective on making a bad habit more difficult.

### Answer
Following is the book's perspective on making a bad habit more difficult:
1. You want to put more steps between you and bad behavior. Adjust the environment according to that.
2. Make bad habits unattractive.
3. Adjust your environment such that doing bad habits is difficult.
4. Make bad habits unsatisfying.

## Question 9
Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

### Answer
One thing that I would like to improve is to take better care of my health.
Following are my plans to achieve it:
- Learning more about the importance of taking care of health to get motivation.
- Making friends and talking with like-minded people.
- Join a gym, as working out with fellow mates is easier.
- Avoid vehicles for walkable distances.
- Make cycling a routine.


## Question 10
Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

### Answer
One habit that I would like to reduce is the usage of mobile phones and social media consumption. Following are my plans to achieve it:
- Use a laptop instead of a mobile for essential stuff as I don't get distracted on the laptop by other unwanted stuff as I do on mobile.
- Mute notifications while working.
- Keep track of mobile screen time so that I am well informed about my usage statistics.
- Engage in other entertainments like cycling, table tennis, etc.