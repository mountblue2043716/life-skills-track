# Grit and Growth Mindset

## Question 1
Paraphrase (summarize) the video in a few lines. Use your own words.

### Answer
Performance from an individual is not directly proportional to the capabilities that they possess. It mostly depends on the mindset. The presenter in the video realized it when she noticed that her students who performed well in her tests were not the only ones with high IQs. The reverse case is also noticeable that not all students with high IQs were not performing well. She later did intensive research on this topic and figured out that the most important predictor of success is grit. Grit is passion and perseverance for very long-term goals.

The best method to build grit in kids is to make them have a growth mindset. This is an idea developed at Stanford University by Carol Dweck. It tells us that the ability to learn is not fixed. It can change with effort. The other thing that can make a person grittier is to assess oneself, test whether we are successful or not, and start over again with the lessons we learned.

## Question 2
What are your key takeaways from the video to take action on?

### Answer
My key takeaways from the video to take action on are:
1. Develop a growth mindset to become a grittier person.
2. Put extra effort as the ability to learn is not fixed, it depends on the effort.
3. Set long-term goals and work for them with consistency.
4. Do a self-assessment and work on the shortcomings.

## Question 3
Paraphrase (summarize) the video in a few lines in your own words.

### Answer
The success of a person relies mostly on the mindset. There are two types of mindsets, fixed and growth mindsets. People with fixed mindset believe that skills and intelligence are set and that they are not in control of their abilities. People with a growth mindset believe that skills and intelligence are grown and developed. They believe that they are in control of their abilities. People with a growth mindset learn, grow and achieve more over time compared to people with a fixed mindset.

Key ingredients to growth are effort, challenges, mistakes, and feedback. People with a growth mindset take up these ingredients positively to improve themself. People with fixed mindset see these things negatively because they think that they cannot improve their abilities.


## Question 4
What are your key takeaways from the video to take action on?

### Answer
My key takeaways from the video to take action on are:
1. Have a growth mindset and work on your abilities to improve them.
2. Instead of looking at performance outcomes, focus on the process of getting better.
3. Be ready to take up challenges.
4. It's ok to make mistakes but learn from them.
5. Get feedback from people and work on them to improve.


## Question 5
What is the Internal Locus of Control? What is the key point in the video?

### Answer
Internal locus of control is a way of thinking that things happening in our life has more to do with internal factors than external factors. It is believing that we are in control of our destiny. The video explains a study conducted in 1998 at Columbia University by Professor Claudia M Mueller on school students. She gave some puzzles to the students. When students solved the puzzle, she said to half of the students that they were able to solve it because they were smart. For the remaining half, she said that their effort and hard work made them do the puzzle. When she gave puzzles to them again, the students who were told that their hard work was the reason for their success performed better. The reason behind this is that these students had a belief that they had control over their success. This is called the internal locus of control. The other half of the students were not performing well and they had low levels of motivation because they had a belief that their success lies in their smartness, which is not in their control. This is called the external locus of control. Having an internal locus of control is the key to staying motivated.

The video later explains a study done on salespersons, which shows that persons who believed that the reason for low sales was their underperformance were able to perform well in the long term. People who blamed external factors were performing low in the long run. This example underlines the importance of believing in the internal locus of control.


## Question 6
Paraphrase (summarize) the video in a few lines in your own words.

### Answer
The steps towards a growth mindset, as described in the video, can be summarised as follows:
1. Believe that you can figure things out and you can get better. This helps you to believe in your efforts to improve yourself.
2. Question the assumptions that limit you. Do not assume that you cannot grow based on today's circumstances.
3. Design a life curriculum so that you have a clear path toward your goals.
4. Do not be discouraged by the hardships faced in the process of growth.
Understand that these are common and they will only make you stronger.

## Question 7
What are your key takeaways from the video to take action on?

### Answer
My key takeaways from the video to take action on are:
1. I should believe in my ability to figure the way to improve myself.
2. Who I am today does not define who I am tomorrow.
3. Work on my skills as there is always a scope for improvement.
4. Set a life curriculum and have a clear path towards my goals.
5. Face hardships with courage as they are there to strengthen me.

## Question 8
What are one or more points that you want to take action on from the manual?

### Answer
These are the points that I want to take action on:
- I will understand the users very well. I will serve them and society by writing rock-solid excellent software.
- I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.





